### Install the Runner

For the most part, this guide will follow the instructions provided in https://docs.gitlab.com/runner/install/linux-repository.html. At this time, the current OS in the provided VMs is CentOS 8 (look at /etc/centos-release to see the current version). Yum is the package manager for CentOS and is the preferred method of installing any packages on CentOS.

Add the official gitlab-runner repository to yum's index and install:

```bash
$ curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" | sudo bash
$ export GITLAB_RUNNER_DISABLE_SKEL=true; sudo -E yum install gitlab-runner
```

The instructions have been updated since I installed the runner, I only ran `sudo yum install gitlab-runner`, but it seems there is a new error which requires disabling skel.

You will then need to enable Docker:

```bash
$ sudo service docker enable
$ sudo service docker start
```

### Connect the Runner

Open your gitlab repository, and go to Settings > CI/CD > Runners (Expand), and you will see the registration details which should include the gitlab url and the registration token. Then, run `gitlab-runner register` to register a new instance of the runner with gitlab:

```bash
$ sudo gitlab-runner register
Runtime platform                                    arch=amd64 os=linux pid=71016 revision=54944146 version=13.10.0
Running in system-mode.                            
                                                   
Enter the GitLab instance URL (for example, https://gitlab.com/):
https://git.linux.iastate.edu/
Enter the registration token:
*******************
Enter a description for the runner:
[****]: DOCKER_1
Enter tags for the runner (comma-separated):
DOCKER
Registering runner... succeeded                     runner=******
Enter an executor: shell, docker+machine, kubernetes, virtualbox, docker-ssh+machine, custom, docker, docker-ssh, parallels, ssh:
docker
Enter the default Docker image (for example, ruby:2.6):
alpine:latest
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded! 
```

Specifically, the description should be able to identify this instance, and the tag should be generic. We will be making one runner using docker, and one using shell to deploy springboot to the server, so using docker for the tag will work well. `alpine:latest` is a good default docker image, but you will override this in the CI settings later.

Create a second runner using the same command, but this will be for deploying using spring-boot using the `shell` executor, so tag and describe it appropriately.

Look in the Runner settings in your gitlab repository to ensure the runners have connected successfully. If they do not both appear, run `sudo service gitlab-runner restart` to force it to update.

Now that the runners have been created, there is one config change that must be made to allow docker to access the internet. Use `vi` or `nano` or any other editor to edit `/etc/gitlab-runner/config.toml`:

```bash
$ sudo vi /etc/gitlab-runner/config.toml
... (:wq to exit)
```

You specifically need to add `network_mode = "host"` as below: You may optionally add `concurrent = n`and `request_concurrency = n` to allow docker to process multiple builds at the same time. Spring boot should have limit set to 1 to prevent multiple concurrent deploys: `limit = 1`

```toml
concurrent = 4
[[runners]]  # Docker
  ...
  request_concurrency = 4
  [runners.docker]
    ...
    network_mode = "host"
[[runners]]  # Deploy
  ...
  limit = 1
```

Now, you can write the CI config for Android and Spring Boot. This config assumes you have three branches: `frontend`, `backend`, and `master`. `frontend` is the essentially the master branch for frontend code, only working merge requests should be merged into it, and it should be ready to merge into master. `backend` is the same for backend code. This allows the backend and frontend to work more independently and only merge into master for a demo and reduce conflicts during normal work. In this setup, the `backend` branch, not `master`, will be the production branch for the backend code which will run on the server.

The overview of this CI config is to build and test any edits to the Backend and Frontend folders using docker, and deploy any builds on the backend branch to the remote server. There are a few built-in stages, this uses build, test, deploy, which are guaranteed to run in that order. `only` specifies to only run CI when changes were made to either the CI config file, or the respective `Frontend` or `Backend` folders. `artifacts` specifies which paths to save as build artifacts. These are the only files kept between build/test/deploy, and are available for download when the build completes. Setting `artifacts` to the entire build folder allows for faster testing as the test stage does not need to rebuild the entire project from scratch, but can use the existing build. `spring-deploy` is the exception, you will need to adjust the copy destination, we created a dummy home folder using our group name to store all run-specific files for the jar, but you can use any accessible folder you wish. Set `GIT_STRATEGY` to `none` to disable downloading the repository since only the artifacts are needed for the deploy stage.

```yaml
# .gitlab-ci.yml

before_script:
  - export GRADLE_USER_HOME=`pwd`/.gradle

android-build:
  image: javiersantos/android-ci:latest
  stage: build
  tags:
    - DOCKER
  script:
    - cd Frontend
    - chmod +x gradlew
    - ./gradlew build
  only:
    changes:
      - .gitlab-ci.yml
      - Frontend/**/*
  artifacts:
    paths:
      - Frontend/app/build/

android-test:
  image: javiersantos/android-ci:latest
  stage: test
  tags:
    - DOCKER
  script:
    - cd Frontend
    - chmod +x gradlew
    - ./gradlew test
  only:
    changes:
      - .gitlab-ci.yml
      - Frontend/**/*

spring-build:
  image: gradle:jdk
  stage: build
  script: 
    - cd Backend
    - gradle assemble
  only:
    changes:
      - .gitlab-ci.yml
      - Backend/**/*
  tags: 
   - DOCKER
  artifacts:
    paths:
      - Backend/build/

spring-test:
  image: gradle:jdk
  stage: test
  script:
    - cd Backend 
    - gradle check
  only:
    changes:
      - .gitlab-ci.yml
      - Backend/**/*
  tags: 
   - DOCKER


spring-deploy:
  image: gradle:jdk
  stage: deploy
  script: 
    - sudo systemctl stop backend
    - cp Backend/build/libs/*.jar /<deploy dir>/current.jar
    - sudo systemctl start backend
  only:
    refs:
      - backend
    changes:
      - .gitlab-ci.yml
      - Backend/**/*
  tags:
    - SHELL_DEPLOY
  dependencies:
    - spring-build
  variables:
    GIT_STRATEGY: none
```

This `.gitlab-ci.yml` file needs to go in the root of the repository, in every branch where CI should run. Preferably, you will merge it into `master`, then merge `master` into `frontend` and `backend`, then those branches into their respective in-progress branches to minimize merge conflicts. Note that this project uses `gradle` for both the front- and back-end of the project. If you are using maven, change the gradle build command to `mvn build` and `mvn test` respectively, and try using the `maven:3-jdk-8` docker image, but this is untested.

### Running the Server

To run the server, you will need a separate `application.properties` file which will contain remote-specific configuration. These servers allow access on port 80, so it makes sense to run your server there from the remote. Here is a sample `application.properties` file that you can adapt to your project. It must go in the same directory as `current.jar`:

```properties
server.port=80

# Springboot configuration for database
spring.datasource.url=jdbc:mysql://localhost:3306/<db name>?useSSL=false&useLegacyDatetimeCode=false&serverTimezone=America/Chicago
spring.datasource.username=<db user>
spring.datasource.password=<db password>
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver

spring.jpa.hibernate.ddl-auto=update
spring.jpa.properties.hibernate.show_sql=true

logging.file.name=logs/latest.log
```

If you use this, you will need to make sure the `logs` folder exists. Logs will be stored there for you to read if needed.

Now that you have the server jar file and `application.properties` configured, you can create a service using `systemd` to run your server in the background. If you have been using `screen` to run your server, this is an alternative that sacrifices interactivity for simplicity. Once you register your service, your server will automatically run and restart upon reboot. Here is an example service which should be saved under `/etc/systemd/system/backend.service`:

```toml
[Unit]
Description=Runs the backend server
After=network.target

[Service]
Restart=no
Type=simple
ExecStart=/bin/bash -c "cd /<deploy dir>/ && rm logs/latest.log && java -jar current.jar"
Environment=

[Install]
WantedBy=multi-user.target 
```

This removes the last run log so you have a clean log entry and can easily find the log from the lastest run. If you want to keep all logs, you may remove that section of the command. Now, run these commands to enable your server to start on boot, and start the server:

```bash
$ sudo systemctl enable backend.service
$ sudo systemctl start backend.service
```

If you look back at the `.gitlab-ci.yml` file from earlier, you may have noticed the deploy commands look rather similar:

```bash
$ sudo systemctl stop backend
$ cp Backend/build/libs/*.jar /<deploy dir>/current.jar
$ sudo systemctl start backend
```

It first stops the server, replaces the server jar, then starts the server back up again. This prevents issues with the jar file disappearing while java is running.



**Congratulations!** You have now successfully set up Continuous Integration and Continuous Deployment for Android Studio and Spring Boot. Here are a few references if you get stuck or want to learn more:

- https://docs.gitlab.com/runner/install/linux-repository.html
- https://github.com/javiersantos/android-ci
- https://hub.docker.com
- https://systemd.io
- https://mysystemd.talos.sh















