Look at the Balance package to start. The main parts in this folder are:

````
BalanceDummyContent: Since backend code is not ready yet, this provides dummy content to fill the list.
BalanceFragment: This you're familiar with and its only job with this view is to attach the adapter to the recycler view.
BalanceItemRecyclerViewAdapter: This "adapts" the recycler view to the balance model. It holds the current data in the view and attaches each piece of data to a particular view as needed.
````

The only real new part is the `BalanceItemRecyclerViewAdapter`. The list we are making is based off of a `RecyclerView`, so you will need to place one of those inside the GUI where you want the list to be, look at `fragment_balance.xml` for an example of that. Make sure these two properties are set on the RecyclerView with the resource id of the view you will make in the next step.

```
app:layoutManager="LinearLayoutManager"
tools:listitem="@layout/fragment_balance"
```

You will need to make a second view which will be the view for each element of the list. I would suggest copying `fragment_balance_item.xml` and modifying it as needed. The main things you should make sure are set: The root view	 should have `layout_width: match_parent` set and `layout_height: wrap_content`. Don't worry about any other weird settings, just layout the view so it looks nice. Once again, use `fragment_balance_item.xml` for an example. Once those two views are set up, make a copy of `BalanceItemRecyclerViewAdapter` and edit everything to work with a list of the appropriate model. Pieces that are required: 

```
public BalanceItemRecyclerViewAdapter(List<Balance> items) {
    super(items, R.layout.fragment_balance); // SET TO THE RIGHT LAYOUT ID
    // Probably no extra setup needed here
}

public ViewHolder createViewHolder(View view) {
    return new ViewHolder(view, this);
    // Nothing else needed here either.
}
```

The `ViewHolder` is a class which will hold and manage the view for an individual item. Think of it as the `Fragment` class for the element in the list. In there, you will define any views that you need access to just like a normal fragment in the constructor. The view you are passed will be the root view of the item xml file you set in. `tools:listitem` and the adapter constructor, so you can `findViewById` on it to get references to your views. The other two methods in here are `setContent` and `onClick`. You should delete all the contents of `setContent` and replace with whatever logic you need to take your item and set the view's properties to show the data. These views *are reused*, so if you might set the text of one view sometimes, but want to leave it at the default other times, you *must* write it every time or it will still be set from the last time this view was used, hence the "Recycler" view. 

The `onClick` method is what will be called when an item is clicked. Balance shows an example of making an alert dialog which you should make for entering item data and similar data entry tasks where appropriate. I wrote a custom view in `balance_alert.xml` and set the text of the label and add a click listener to validate the data on the submit button in the method. Important note: If you change a model for any reason, you *must* call `notifyUpdate()` on its ViewHolder or one of the `notify` methods on the adapter. If you delete a model, call `notifyRemove()` instead on the ViewHolder. I assume you'll want to do some api call in the ViewHolder click callback, so if you do, wait to remove the alert dialog until the request suceeds, and call `notifyUpdate()` from within the api callback after setting the data item. I have not tested, but setting `item` to a new instance of the item might also work.

That's all it takes to get nice-looking lists working! There's a bit more with coloring and stuff that happens in `GenericItemRecyclerViewAdapter` that I wrote if you want to take a look at this class you're extending.

