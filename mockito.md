

## IntelliJ Auto-Generation

IntelliJ has a great feture built-in for creating JUnit tests which will create the template automagically. If you open the class you want to test, put the text cursor in the class name, and hold `alt` while you press `enter`, then this menu will appear:

![](assets/1.png)

Click `Create Test`, and it will open a new JUnit test dialog. Usually, you will want to select all the methods you will want to test as well as `setUp/@Before` so you can do some initialization that will run before each test. The defaults for everything else should work, and it should automatically put the test class in the tests folder in your project.

![](assets/2.png)

## Setup

Now it's time to start using Mockito to mock this UserService. You will want to add two import statements if they are not already there:

```java
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
```

These will allow you to run the `assert*` and mockito methods without `Mockito` or `Assertions` before each method call. Here is an example of a `UserServiceTest` to reference throughout the explanations below:

```java
...

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UserServiceTest {

    private final List<User> testUsers = Arrays.asList(
            new User("User Name 1", "user1@iastate.edu"),
            new User("User Name 2", "user2@iastate.edu"),
            new User("User Name 3", "user3@iastate.edu")
    );

    @Mock private UserRepository userRepo;
    @Mock private NamedParameterJdbcTemplate jdbc;
    private UserService userService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
      // OR
        userRepo = mock(UserRepository.class);
        jdbc = mock(NamedParameterJdbcTemplate.class);
      // END OR

        userService = new UserService(userRepo, jdbc);
    }

    @Test
    void getUsers() {
        when(userRepo.findAll())
                .thenReturn(testUsers)
                .thenReturn(null);

        assertEquals(testUsers, userService.getUsers());
        assertNull(userService.getUsers());
        verify(userRepo, times(2)).findAll();
    }

    @Test
    void getUser() {
        Optional<User> result = Optional.of(testUsers.get(1));
        when(userRepo.findById(anyLong()))
                .thenReturn(result);

        assertEquals(result, userService.getUser(42));
        verify(userRepo).findById(42L);
    }

    @Test
    void getUserByEmail() {
        Optional<User> result = Optional.of(testUsers.get(2));
        when(userRepo.findByEmail(anyString()))
                .thenReturn(result);

        assertEquals(result, userService.getUser("user1@iastate.edu"));
        verify(userRepo).findByEmail("user1@iastate.edu");
    }

    @Test
    void getConnections() {
        //noinspection unchecked
        when(jdbc.queryForList(anyString(), anyMap(), any(Class.class)))
                .thenReturn(testUsers.subList(0, 1));

        assertEquals(testUsers.subList(0, 1), userService.getConnections(testUsers.get(0)));
        //noinspection unchecked
        verify(jdbc).queryForList(anyString(), anyMap(), any(Class.class));
    }
}
```

Little bit of background, our project uses OAuth to log in users, so for the sake of this example, assume OauthId is just another string property on the User model. Let's start by taking a look at the setup code. First, I created a list of User instances that I will use for testing. Keeping this as a property allows me to access this list from both the setup and test code to see if the list matches. Since nothing should be copying or modifying this list, it should be the safe to use in an `assertEquals()` call to see if a method returned the list correctly in its entirety. Second, I defined the mock objects using `@Mock`. In class, we used `mock(Mocked.class)`, in the setup method. This does the same thing with less code. I also declare an instance of the UserService as a private property for later use.

The setup method runs `MockitoAnnotations.openMocks(this)` which essentially sets each `@Mock` property to `mock(Mocked.class)`. This could just as well be written without the `@Mock` annotations and setting each mock individually here instead. Next, I created the user service, passing it the two mocked instances. Depending upon your form of dependency injection, your code might look different here. Our project is using constructor injection (you have to pass the dependencies to be injected in the constructor), setter injection or `@Autowired` properties will look different.

## When

Each test case defines the mock methods it will be using with the `when(...)` method. To mock a method, you call it exactly as you expect it to be called and put the return value in the `when` method. After `when`, you can add any return values or exceptions you want the mock to return or throw. Here are a few examples with explanations:

---

When the `userRepo.findAll()` method is called with no arguments, return `testUsers` the first time, then return `null` the second time:

```java
when(userRepo.findAll())
     .thenReturn(testUsers)
     .thenReturn(null);
```

When the `userRepo.findById()` method is called with any long as the only argument, always return `result`. `anyLong()` can be replaced with a real `long` value, and it will only return `result` when the arguments match:

```java
when(userRepo.findById(anyLong()))
      .thenReturn(result);
```

When the `userRepo.findByOauthId()` method is called with an id of `invalid`, throw an `InvalidArgumentException`:

```java
when(userRepo.findByOauthId("Invalid")
      .thenThrow(InvalidArgumentException.class);
```

When the `userRepo.echo()` method is called, return its first argument's value:

```java
when(userRepo.echo(anyString()))
      .then(AdditionalAnswers.returnsFirstArg());
```

When the `userRepo.testUser()` method is called with any User instance, return void (Note different syntax for returning void):

```java
doNothing().when(userRepo).echo(any(User.class));
```

---

## Assert

You can then use a combination of JUnit `assert` and Mockito `verify` method calls to ensure your code is operating correctly. `assert` calls are fairly straightforward. Put the expected value first, then the actual value second, and it will compare the two:

```java
assertEquals("expected", users.getExpected());
assertTrue(users.getTrue());
assertThrows(IllegalArgumentException.class, () -> users.throwException());
assertThrows(IllegalArgumentException.class, users::throwException);
assertThrows(IllegalArgumentException.class, new Executable() {
    @Override
    public void execute() throws Throwable {
        users.throwException();
    }
});
```

The third is a little more confusing because the second argument is not the result of the method, but an instance of Executable which should throw an exception. The last three calls are equivalent, the third uses a java lambda expression, the fourth uses a method  reference, and the fifth is the longhand interface instantiation. The first two are newer syntaxes, so they might not work in your JDK, but they're much shorter if they do compile.

## Verify

For `verify`, you insert the mock instance as the first parameter, and an optional number of times the function should be called. Then pretend the return value of verify is the mock, and call it as your program should (or should not):

```java
verify(users).findById(anyLong()); 						// Defaults to 1 time
verify(users, times(2)).findById(anyLong());  // Should be called twice
verify(users, never()).findById(anyLong());   // Should never be called
```

Since beforeEach is called before each test, you do not need to reset these counts except in the middle of a test method. If you need to reset in the middle of a test method, you can call `clearInvocations(mock);` to reset the counts for all method calls:

```java
/** Do stuff with users that calls findById once. **/
verify(users).findById(anyLong()); // OK
verify(users, never()).findById(anyLong()); // FAIL Count is 1, not 0

clearInvocations(users); 					 // Count is now 0
verify(users, never()).findById(anyLong()); // OK
```

---

And that's it! One of our user methods uses a raw JDBC SQL query, so I left that in there so there is an example of something more complex. It's all the same concepts as above, just with more complicated arguments. Note that the last argument to `queryForList` requires a class of the type of object you want returned. `User.class.class`doesn't make sense because `.class` can only be used on Classes themselves. `User.class.getClass()` makes IntelliJ unhappy because the value of that would be essentially `Class<User>.class`. Class instances cannot be generic, so that is equivalent to `Class.class` and seems to be the best way of defining that mock.

## Resources 

- https://stackoverflow.com/questions/30081161/mockito-does-verify-method-reboot-number-of-times
- https://site.mockito.org/javadoc/current/org/mockito/MockitoAnnotations.html
- https://site.mockito.org/javadoc/current/org/mockito/Mockito.html
- https://www.springboottutorial.com/spring-boot-unit-testing-and-mocking-with-mockito-and-junit

